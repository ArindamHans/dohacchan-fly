class LoadScene extends Phaser.Scene {
	constructor() {
		super('LoadScene');
	}

	preload() {
		this.load.on('progress', (value) => {
			console.log(value * 100 + ' %');
		});

		this.load.on('complete', () => {
			this.scene.start('GameScene');
		});

		this.load.bitmapFont('font', 'assets/fonts/font.png', 'assets/fonts/font.fnt');
		this.load.multiatlas(
			'gamesprites',
			'assets/sprites/gamesprites.json',
			'assets/sprites/'
		);
		// this.load.image('background-day', 'assets/sprites/background-day.png');
		// this.load.image('background-night', 'assets/sprites/background-night.png');
		// this.load.image('base', 'assets/sprites/base.png');
		// this.load.image('bird-downflap', 'assets/sprites/bird-downflap.png');
		// this.load.image('bird-upflap', 'assets/sprites/bird-upflap.png');
		// this.load.image('intro', 'assets/sprites/message.png');
		// this.load.image('gameover', 'assets/sprites/gameover.png');
		// this.load.image('pipe', 'assets/sprites/pipe.png');
		// this.load.image('replay', 'assets/sprites/replay.png');
		// this.load.image('scoreboard-best', 'assets/sprites/scoreboard-best.png');
		// this.load.image('scoreboard-streak', 'assets/sprites/scoreboard-streak.png');
	}

	create() {
		var frameNames = this.anims.generateFrameNames('gamesprites', {
			prefix: 'bird-',
			start: 1,
			end: 2,
			zeroPad: 2,
			suffix: '.png',
		});

		this.anims.create({
			key: 'fly',
			frames: frameNames,
			frameRate: 10,
			repeat: -1,
		});
	}
}

export default LoadScene;
