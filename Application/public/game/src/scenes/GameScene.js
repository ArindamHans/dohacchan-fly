class GameScene extends Phaser.Scene {
	constructor() {
		super('GameScene');
	}

	create() {
		this.score = {
			pts: 0,
			textObject: this.add.bitmapText(
				this.game.config.width / 2.1,
				20,
				'font',
				'0',
				40
			),
		};
		this.score.textObject.setDepth(4);
		this.gameStarted = false;
		this.finishedGame = false;

		this.gameLogs = [];

		this.bgRandom = Phaser.Math.Between(0, 1);
		var bgDayOrNight;
		switch (this.bgRandom) {
			case 0:
				bgDayOrNight = 'background-day.png';
				break;
			case 1:
				bgDayOrNight = 'background-night.png';
				break;
		}

		this.bg = this.add.tileSprite(0, 0, 300, 512, 'gamesprites', bgDayOrNight);
		this.bg.setOrigin(0, 0);
		this.bg.setDepth(0);
		this.bg.setInteractive();

		this.base = this.add.tileSprite(
			0,
			this.game.config.height,
			600,
			50,
			'gamesprites',
			'base.png'
		);
		this.base.setDepth(4);
		this.physics.add.existing(this.base, false);
		this.base.body.allowGravity = false;
		this.base.body.setCollideWorldBounds(true);

		this.bird = this.physics.add.sprite(75, 300).play('fly');
		this.bird.setCircle(13.9, 7, 1.5);
		this.bird.setOrigin(0.5, 0.7);
		this.bird.body.allowGravity = false;
		// this.bird.body.setCollideWorldBounds(true);
		this.bird.setDepth(3);
		this.physics.add.collider(this.base, this.bird, this.gameOver, null, this);

		this.invisWall = this.physics.add.sprite(0, -47, 'gamesprites', 'base.png');
		this.invisWall.scaleX = this.game.config.width;
		this.invisWall.setDepth(4);
		this.invisWall.flipY = true;
		this.invisWall.body.allowGravity = false;
		this.base.body.setCollideWorldBounds(true);
		this.physics.add.collider(this.invisWall, this.bird, this.gameOver, null, this);

		this.pipes = this.physics.add.group();
		this.pipes.setDepth(2);
		this.physics.add.overlap(this.pipes, this.bird, this.gameOver, null, this);

		this.zonesScore = this.physics.add.group();
		this.physics.add.overlap(
			this.bird,
			this.zonesScore,
			this.incrementScore,
			null,
			this
		);

		this.intro = this.add.image(
			this.game.config.width / 2,
			this.game.config.height / 2,
			'gamesprites',
			'gamestart.png'
		);
		this.intro.setDepth(10);

		this.bg.on('pointerdown', () => {
			this.gameStarted ? this.jump() : this.startGame();
		});

		this.time.addEvent({
			delay: 1500,
			callback: this.addOnePipe,
			callbackScope: this,
			loop: true,
		});
		this.time.addEvent({
			delay: 2000,
			callback: this.deletePipes,
			callbackScope: this,
			loop: true,
		});
		this.changeBgEvent = this.time.addEvent({
			delay: 20000,
			callback: this.changeBackground,
			callbackScope: this,
			loop: true,
		});

		// this.time.addEvent({
		// 	delay: 3000,
		// 	callback: this.addShapes,
		// 	callbackScope: this,
		// 	loop: true,
		// });

		// this.trianglePositionX = 300;
	}

	update() {
		if (!this.finishedGame) {
			this.bg.tilePositionX += 0.25;
			this.base.tilePositionX += 2.5;
			//this.trianglePositionX += 2.5;randomHeight

			if (this.gameStarted) {
				this.pipes.getChildren().forEach((pipe) => {
					pipe.x -= 2.5;
				});
				this.zonesScore.getChildren().forEach((zoneScore) => {
					zoneScore.x -= 2.5;
				});

				if (this.bird.angle < 20 && this.gameStarted) {
					this.bird.angle += 1;
				}
			}
		}

		// if (!this.finishedGame && this.gameStarted) {
		// 	this.pipes.getChildren().forEach((pipe) => {
		// 		pipe.x -= 2.5;
		// 	});
		// 	this.zonesScore.getChildren().forEach((zoneScore) => {
		// 		zoneScore.x -= 2.5;
		// 	});

		// 	if (this.bird.angle < 20 && this.gameStarted) {
		// 		this.bird.angle += 1;
		// 	}
		// }
	}

	// addShapes() {
	// 	if (this.gameStarted && !this.finishedGame) {
	// 		// let triangleNum,
	// 		// 	crossNum = Phaser.Math.Between(0, 3);
	// 		// let squareNum,
	// 		// 	circleNum = Phaser.Math.Between(0, 2);

	// 		//triangle
	// 		for (let i = 0; i <= Phaser.Math.Between(0, 3); i++) {
	// 			let config = {
	// 				side: Phaser.Math.Between(20, 50),
	// 				spawnY: Phaser.Math.Between(0, 450),
	// 				angularSpeed: Phaser.Math.Between(20, 40),
	// 			};

	// 			let randomSpawnX = this.trianglePositionX + Phaser.Math.Between(-20, 30);

	// 			let shape = this.add.triangle(
	// 				randomSpawnX,
	// 				config.spawnY,
	// 				0,
	// 				config.side,
	// 				config.side,
	// 				config.side,
	// 				config.side / 2,
	// 				0,
	// 				0xefc53f
	// 			)
	// 			.setDepth(1);

	// 			let border = this.add
	// 				.triangle(
	// 					randomSpawnX,
	// 					config.spawnY - 10,
	// 					0,
	// 					config.side,
	// 					config.side,
	// 					config.side,
	// 					config.side / 2,
	// 					0
	// 				)
	// 				.setStrokeStyle(2, 0x9966ff)
	// 				.setDepth(1);

	// 			this.tweens.add({
	// 				targets: [shape, border],
	// 				ease: 'Linear',
	// 				props: {
	// flappy bird clones					x: { value: '-=400', duration: 1000 },
	// 					angle: {
	// 						value: '180',
	// 						duration: Phaser.Math.Between(2700, 3300),
	// 					},
	// 				},
	// 			});
	// 		}

	// 		// //cross
	// 		// for (let i = 0; i <= Phaser.Math.Between(0, 3); i++) {}

	// 		// //circle
	// 		// for (let i = 0; i <= Phaser.Math.Between(0, 2); i++) {}

	// 		// //square
	// 		// for (let i = 0; i <= Phaser.Math.Between(0, 2); i++) {}
	// 	}
	// }

	addOnePipe() {
		if (this.gameStarted && !this.finishedGame) {
			this.gap = 120;
			this.randomHeightTop = Phaser.Math.Between(50, 312);
			let calculHeightBottom =
				this.game.config.height -
				this.base.height -
				this.randomHeightTop -
				this.gap;
			let pBottom = this.add
				.tileSprite(300, 464, 52, calculHeightBottom, 'gamesprites', 'pipe.png')
				.setOrigin(0.5, 1);
			this.physics.add.existing(pBottom, false);
			this.pipes.add(pBottom);
			pBottom.body.setImmovable();
			pBottom.body.setAllowGravity(false);

			let pTop = this.add
				.tileSprite(300, this.randomHeightTop, 52, 320, 'gamesprites', 'pipe.png')
				.setFlipY(true)
				.setOrigin(0.5, 1);
			this.physics.add.existing(pTop, false);
			this.pipes.add(pTop);
			pTop.body.setImmovable();
			pTop.body.setAllowGravity(false);

			let zoneScore = this.add.zone(300 + pBottom.width / 2, 0, 1, 1400);
			this.zonesScore.add(zoneScore);
			zoneScore.setDepth(0);
			this.physics.world.enable(zoneScore);
			zoneScore.body.setAllowGravity(false);
			zoneScore.body.moves = false;
		}
	}

	deletePipes() {
		if (this.gameStarted && !this.finishedGame) {
			this.pipes.getChildren().forEach((pipe) => {
				if (pipe.x < -pipe.width) {
					pipe.destroy();
				}
			});
		}
	}

	updateLogs() {
		var zoneData = [];
		this.zonesScore.getChildren().forEach((zoneScore) => {
			zoneData.push({ _x: zoneScore.x, _height: zoneScore.height });
		});

		var pipeData = [];
		this.pipes.getChildren().forEach((pipe) => {
			pipeData.push({ _x: pipe.x, _heightTop: this.randomHeightTop });
		});

		var log = {
			_score: this.score.pts,
			_playerData: { _x: this.bird.x, _y: this.bird.y },
			_zoneData: zoneData,
			_pipeData: pipeData,
			_gap: this.gap,
		};
		this.gameLogs.push(log);
	}

	jump() {
		if (!this.finishedGame) {
			this.bird.setVelocityY(-250);
			this.bird.angle = -20;
		}
	}

	incrementScore(bird, zoneScore) {
		this.score.pts++;
		this.score.textObject.setText('' + this.score.pts);
		zoneScore.destroy();
	}

	changeBackground() {
		if (this.bgRandom == 1) {
			this.bgRandom = 0;
			this.bg.setTexture('gamesprites', 'background-day.png');
		} else {
			this.bgRandom = 1;
			this.bg.setTexture('gamesprites', 'background-night.png');
		}
	}

	startGame() {
		this.gameStarted = true;
		this.intro.visible = false;
		this.bird.body.allowGravity = true;
		this.updateLogsEvent = this.time.addEvent({
			delay: 7000,
			callback: this.updateLogs,
			callbackScope: this,
			loop: true,
		});
	}

	gameOver() {
		if (!this.finishedGame) {
			this.finishedGame = true;
			this.changeBgEvent.remove();
			this.score.textObject.visible = false;
			this.bird.anims.pause();
			var gameData = { '_finalScore': this.score.pts, '_logs': this.gameLogs };
			this.registry.set('gameData', gameData);
			this.game.scene.start('GameOverScene');
		}
	}
}

export default GameScene;
