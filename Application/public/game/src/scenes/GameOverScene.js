class GameOverScene extends Phaser.Scene {
	constructor() {
		super('GameOverScene');
	}

	create() {
		this.gameover = this.add.image(
			this.game.config.width / 2,
			this.game.config.height / 7,
			'gamesprites',
			'gameover.png'
		);
		this.gameover.setDepth(4);

		this.currentScoreText = this.add.bitmapText(
			this.game.config.width / 2.23,
			this.game.config.height / 3.7,
			'font',
			this.registry.get('gameData')._finalScore,
			60
		);
		this.currentScoreText.setDepth(4);

		this.addScore();
	}

	addScore() {
		fetch('/addScore', {
			method: 'POST',
			body: JSON.stringify(this.registry.get('gameData')),
			headers: { 'Content-Type': 'application/json' },
		})
			.then((res) => res.json())
			.then((data) => {
				// Scoreboard Headings
				this.scoreboard_best = this.add.image(
					this.game.config.width / 5,
					this.game.config.height / 2,
					'gamesprites',
					'scoreboard-best.png'
				);
				this.scoreboard_best.setDepth(4);

				this.scoreboard_streak = this.add.image(
					this.game.config.width / 1.3,
					this.game.config.height / 2,
					'gamesprites',
					'scoreboard-streak.png'
				);
				this.scoreboard_streak.setDepth(4);

				// Scoreboard Texts
				this.BestScoreText = this.add.bitmapText(
					this.game.config.width / 6.2,
					this.game.config.height / 2 + 22,
					'font',
					data.bestScore,
					28
				);
				this.BestScoreText.setDepth(4);

				this.StreakCountText = this.add.bitmapText(
					this.game.config.width / 1.3 - 5,
					this.game.config.height / 2 + 22,
					'font',
					data.streakCount,
					28
				);
				this.StreakCountText.setDepth(4);

				this.replay = this.add
					.image(
						this.game.config.width / 2,
						this.game.config.height / 1.3,
						'gamesprites',
						'restart-button.png'
					)
					.setInteractive();
				this.replay.setDepth(4);
				this.replay.setInteractive = true;

				this.replay.on('pointerdown', () => {
					this.replay.disableInteractive();
					fetch('/newGame', {
						method: 'POST',
						body: JSON.stringify({ requestFrom: 'gameOver' }),
						headers: { 'Content-Type': 'application/json' },
					})
						.then((res) => {
							console.log('res.status = ', res.status);
							if (res.status != 200) {
								console.log('res.status != 200');
								window.location.replace('/wait');
							}
							this.scene.stop();
							this.game.scene.stop('GameOverScene');
							this.scene.launch('GameScene');
						})
						.catch((error) => {
							console.log(error);
						});
				});
			})
			.catch(function (err) {
				console.log('Request failure: ', err);
			});
	}
}

export default GameOverScene;
