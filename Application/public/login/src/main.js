require('./site.css');
import 'normalize.css';
import $ from 'jquery';

setTimeout(function () {
	let viewheight = $(window).height();
	let viewwidth = $(window).width();
	let viewport = document.querySelector('meta[name=viewport]');
	viewport.setAttribute(
		'content',
		'height=' + viewheight + 'px, width=' + viewwidth + 'px, initial-scale=1.0'
	);
}, 100);

$(document).ready(function () {
	$('#loginForm').on('submit', function (e) {
		e.preventDefault();

		$.ajax({
			type: 'POST',
			url: '/login',
			data: $('#loginForm').serialize(),
			success: function (xhr) {
				window.location.replace('/game');
			},
			error: function (xhr) {
				var response = xhr.responseJSON;
				$('#snackbar').html(response.message).addClass('show');
				setTimeout(function () {
					$('#snackbar').removeClass('show').addClass('');
				}, 3000);
			},
		});
	});
});
