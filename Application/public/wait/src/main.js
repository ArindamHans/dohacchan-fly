require('./site.css');
import 'normalize.css';
import $ from 'jquery';

$(document).ready(function () {
	const second = 1000,
		minute = second * 60,
		hour = minute * 60;

	var endTime;
	$.get('/getStreak', function (data) {
		if (data.streak._gamesPlayed < 5) window.location.replace('/game');
		let endDate = new Date(data.streak._streakStartTime);
		endDate.setHours(endDate.getHours() + 1);
		endTime = endDate.getTime();
	});

	$(function () {
		let x = setInterval(function () {
			let now = new Date().getTime(),
				distance = endTime - now;

			let minLeft = Math.floor((distance % hour) / minute);
			let secLeft = Math.floor((distance % minute) / second);

			document.getElementById('timer').innerText = minLeft + ':' + secLeft;

			//do something later when date is reached
			if (distance < 0) {
				clearInterval(x);
				window.location.replace('/game');
			}
		}, second);
	});
});
