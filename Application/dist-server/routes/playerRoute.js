'use strict';

var _path = _interopRequireDefault(require("path"));

var _express = _interopRequireDefault(require("express"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

module.exports = function (app) {
  var playerHandlers = require('../controllers/playerController.js'); // app.all('*', playerHandlers.checkDevice);


  app.get('/', playerHandlers.loginRequired, playerHandlers.streakValid, function (req, res, next) {
    res.redirect('/game');
  });
  app.get('/game', playerHandlers.loginRequired, playerHandlers.streakValid);
  app.use('/game', _express["default"]["static"](_path["default"].join(__dirname, '../../dist-client/game')));
  app.get('/wait', function (req, res, next) {
    next();
  });
  app.use('/wait', _express["default"]["static"](_path["default"].join(__dirname, '../../dist-client/wait')));
  app.get('/getStreak', playerHandlers.getGameStreak);
  app.post('/newGame', playerHandlers.streakValid, function (req, res, next) {
    console.log('post(/newGame)');
    res.sendStatus(200);
  });
  app.get('/login', function (req, res, next) {
    next();
  });
  app.use('/login', _express["default"]["static"](_path["default"].join(__dirname, '../../dist-client/login')));
  app.post('/login', playerHandlers.login);
  app.post('/addScore', playerHandlers.addScore);
}; // express.static(path.join(__dirname, '../../client/views/game')
// app.route('/login')
// 		.get((req, res, next) => {
// 			res.sendFile('index.html', {
// 				root: path.join(__dirname, '../../client/views/other/'),
// 			});
// 		})
// 		.post(playerHandlers.login);
// app.use('/', (req, res, next) => {
// 	res.sendFile('index.html', {
// 		root: path.join(__dirname, '../../client/views/other/'),
// 	});
// });