"use strict";

var _jsonwebtoken = _interopRequireDefault(require("jsonwebtoken"));

var _mongoose = _interopRequireDefault(require("mongoose"));

var _moment = _interopRequireDefault(require("moment"));

var _dotenv = _interopRequireDefault(require("dotenv"));

var _regeneratorRuntime = require("regenerator-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var Player = _mongoose["default"].model('Player');

_dotenv["default"].config();

exports.login = function (req, res) {
  Player.findOne({
    _username: req.body.name.toLowerCase(),
    _password: req.body.password
  }, function (err, player) {
    if (err) {
      return res.status(502).json({
        message: 'Bad Gateway'
      });
    }

    if (!player) {
      return res.status(401).json({
        message: "Authentication failed. Invalid username or password."
      });
    }

    var token = _jsonwebtoken["default"].sign({
      name: req.body.name.toLowerCase(),
      password: req.body.password
    }, process.env.ACCESS_TOKEN_SECRET);

    var cookieOptions = {
      httpOnly: true
    };
    res.cookie('token', token, cookieOptions);
    res.redirect(200, '/game');
  });
};

exports.loginRequired = function (req, res, next) {
  if (req.player) {
    next();
  } else {
    res.redirect('/login');
  }
};

exports.streakValid = /*#__PURE__*/function () {
  var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(req, res, next) {
    var playerDoc, now;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.next = 2;
            return Player.findOne({
              _username: req.player.name,
              _password: req.player.password
            });

          case 2:
            playerDoc = _context.sent;

            if (playerDoc) {
              _context.next = 8;
              break;
            }

            console.log('NO Player');
            res.send('No Player Header');
            _context.next = 36;
            break;

          case 8:
            if (!(playerDoc._gameStreak._gamesPlayed < process.env.STREAK_MAX)) {
              _context.next = 12;
              break;
            }

            next();
            _context.next = 36;
            break;

          case 12:
            if (!(playerDoc._gameStreak._gamesPlayed > process.env.STREAK_MAX - 1)) {
              _context.next = 34;
              break;
            }

            now = (0, _moment["default"])();

            if (!(now.diff(playerDoc._gameStreak._streakStartTime, 'seconds') > 3600)) {
              _context.next = 30;
              break;
            }

            _context.prev = 15;
            playerDoc._gameStreak._gamesPlayed = 0;
            playerDoc._gameStreak._streakStartTime = now;
            _context.next = 20;
            return playerDoc.save();

          case 20:
            next();
            _context.next = 27;
            break;

          case 23:
            _context.prev = 23;
            _context.t0 = _context["catch"](15);
            console.error(_context.t0);
            res.status(504).json({
              message: 'Gateway Timeout'
            });

          case 27:
            next();
            _context.next = 32;
            break;

          case 30:
            if (req.body.requestFrom) {
              res.sendStatus(401);
            }

            res.redirect('/wait');

          case 32:
            _context.next = 36;
            break;

          case 34:
            if (req.body.requestFrom) {
              res.sendStatus(401);
            }

            res.redirect('/wait');

          case 36:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[15, 23]]);
  }));

  return function (_x, _x2, _x3) {
    return _ref.apply(this, arguments);
  };
}();

exports.getGameStreak = /*#__PURE__*/function () {
  var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(req, res, next) {
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            Player.findOne({
              _username: req.player.name,
              _password: req.player.password
            }, function (err, result) {
              if (err) res.status(504).json({
                message: 'Gateway Timeout'
              });
              res.json({
                streak: result._gameStreak
              });
            });

          case 1:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));

  return function (_x4, _x5, _x6) {
    return _ref2.apply(this, arguments);
  };
}();

exports.checkDevice = function (req, res, next) {
  var ua = req.headers['user-agent'].toLowerCase(),
      isMobile = !!/Mobile|iP(hone|od|ad)|Android|BlackBerry|IEMobile|Kindle|NetFront|Silk-Accelerated|(hpw|web)OS|Fennec|Minimo|Opera M(obi|ini)|Blazer|Dolfin|Dolphin|Skyfire|Zune/i.test(ua);

  if (isMobile) {
    next();
  } else {
    res.status(404).json({
      message: 'Not found'
    });
  }
};

exports.addScore = /*#__PURE__*/function () {
  var _ref3 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(req, res, next) {
    return regeneratorRuntime.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            if (req.body) {
              Player.findOneAndUpdate({
                _username: req.player.name,
                _password: req.player.password
              }, {
                $push: {
                  _scores: {
                    $each: [req.body._finalScore],
                    $sort: -1,
                    $slice: 10
                  },
                  _gameLogs: {
                    $each: [req.body],
                    $sort: {
                      _finalScore: -1
                    },
                    $slice: 3
                  }
                },
                $inc: {
                  _totalGamesPlayed: 1,
                  '_gameStreak._gamesPlayed': 1
                }
              }, {
                safe: true,
                upsert: true,
                "new": true,
                minimize: false
              }, function (err, result) {
                if (err) res.status(504).json({
                  message: 'Gateway Timeout'
                });
                res.status(200).json({
                  bestScore: result._scores[0],
                  streakCount: result._gameStreak._gamesPlayed
                });
              });
            } else res.status(404).json({
              message: 'Not found'
            });

          case 1:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3);
  }));

  return function (_x7, _x8, _x9) {
    return _ref3.apply(this, arguments);
  };
}();