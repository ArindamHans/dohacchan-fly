"use strict";

var _mongoose = _interopRequireDefault(require("mongoose"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var Schema = _mongoose["default"].Schema;
var PlayerSchema = new Schema({
  _username: {
    type: String,
    required: true,
    match: /^((.+?)#\d{4})$/
  },
  _realname: {
    type: String,
    match: /^((.+?)#\d{4})$/
  },
  _password: {
    type: String,
    required: true
  },
  _scores: {
    type: [Number],
    "default": new Array(10).fill(0)
  },
  _gameLogs: [{
    _finalScore: Number,
    _logs: [{
      _score: Number,
      _playerData: {
        _x: Number,
        _y: Number
      },
      _zoneData: [{
        _x: Number,
        _height: Number
      }],
      _pipeData: [{
        _x: Number,
        _heightTop: Number
      }],
      _gap: Number
    }]
  }],
  _totalGamesPlayed: {
    type: Number,
    "default": 0
  },
  _gameStreak: {
    _streakStartTime: {
      type: Date,
      "default": Date.now()
    },
    _gamesPlayed: {
      type: Number,
      "default": 0
    }
  }
});

_mongoose["default"].model('Player', PlayerSchema, 'players');