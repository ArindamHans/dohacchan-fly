"use strict";

var _express = _interopRequireDefault(require("express"));

var _cors = _interopRequireDefault(require("cors"));

var _bodyParser = _interopRequireDefault(require("body-parser"));

var _dotenv = _interopRequireDefault(require("dotenv"));

var _mongoose = _interopRequireDefault(require("mongoose"));

var _jsonwebtoken = _interopRequireDefault(require("jsonwebtoken"));

var _cookieParser = _interopRequireDefault(require("cookie-parser"));

var _regeneratorRuntime = _interopRequireDefault(require("regenerator-runtime"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var Player = require('./models/playerModel');

var routes = require('./routes/playerRoute');

_dotenv["default"].config();

var app = (0, _express["default"])();
var port = process.env.PORT || 4000;
var uri = "mongodb+srv://dohacchan-float67:".concat(process.env.DB_PASS, "@dohacchan-fly.ev9sz.mongodb.net/").concat(process.env.DB_NAME, "?retryWrites=true&w=majority"); // Connect to DB

try {
  _mongoose["default"].connect(uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
  });
} catch (e) {
  console.error(e);
}

app.use((0, _cors["default"])());
app.use(_bodyParser["default"].urlencoded({
  extended: false
}));
app.use(_bodyParser["default"].json());
app.use((0, _cookieParser["default"])()); // Middleware to JWT header

app.use(function (req, res, next) {
  if (req.cookies.token) {
    _jsonwebtoken["default"].verify(req.cookies.token, process.env.ACCESS_TOKEN_SECRET, function (err, decode) {
      if (err) {
        console.log('invalied cookie');
        res.sendStatus(500);
      }

      req.player = decode;
      next();
    });
  } else {
    req.player = undefined;
    next();
  }
});
routes(app);
app.use(function (req, res) {
  res.status(404);
});
app.listen(port, function () {
  return console.log("Example app listening on port ".concat(port, "!"));
});