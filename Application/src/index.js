import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import dotenv from 'dotenv';
import mongoose from 'mongoose';
import jwt from 'jsonwebtoken';
import cookieParser from 'cookie-parser';
import regeneratorRuntime from 'regenerator-runtime';
var Player = require('./models/playerModel');
var routes = require('./routes/playerRoute');

dotenv.config();
const app = express();
const port = process.env.PORT || 4000;
const uri = `mongodb+srv://dohacchan-float67:${process.env.DB_PASS}@dohacchan-fly.ev9sz.mongodb.net/${process.env.DB_NAME}?retryWrites=true&w=majority`;

// Connect to DB
try {
	mongoose.connect(uri, {
		useNewUrlParser: true,
		useUnifiedTopology: true,
		useFindAndModify: false,
	});
} catch (e) {
	console.error(e);
}

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cookieParser());

// Middleware to JWT header

app.use(function (req, res, next) {
	if (req.cookies.token) {
		jwt.verify(req.cookies.token, process.env.ACCESS_TOKEN_SECRET, function (
			err,
			decode
		) {
			if (err) {
				console.log('invalied cookie');
				res.sendStatus(500);
			}
			req.player = decode;
			next();
		});
	} else {
		req.player = undefined;
		next();
	}
});

routes(app);

app.use(function (req, res) {
	res.status(404);
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
