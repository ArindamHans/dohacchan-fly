// import express from 'express';
import jwt from 'jsonwebtoken';
import mongoose from 'mongoose';
import moment from 'moment';
import dotenv from 'dotenv';
import { async } from 'regenerator-runtime';

const Player = mongoose.model('Player');

dotenv.config();

exports.login = (req, res) => {
	Player.findOne(
		{
			_username: req.body.name.toLowerCase(),
			_password: req.body.password,
		},
		function (err, player) {
			if (err) {
				return res.status(502).json({
					message: 'Bad Gateway',
				});
			}
			if (!player) {
				return res.status(401).json({
					message: `Authentication failed. Invalid username or password.`,
				});
			}
			const token = jwt.sign(
				{ name: req.body.name.toLowerCase(), password: req.body.password },
				process.env.ACCESS_TOKEN_SECRET
			);
			const cookieOptions = {
				httpOnly: true,
			};
			res.cookie('token', token, cookieOptions);
			res.redirect(200, '/game');
		}
	);
};

exports.loginRequired = function (req, res, next) {
	if (req.player) {
		next();
	} else {
		res.redirect('/login');
	}
};

exports.streakValid = async function (req, res, next) {
	let playerDoc = await Player.findOne({
		_username: req.player.name,
		_password: req.player.password,
	});

	if (!playerDoc) {
		console.log('NO Player');
		res.send('No Player Header');
	} else {
		if (playerDoc._gameStreak._gamesPlayed < process.env.STREAK_MAX) next();
		else if (playerDoc._gameStreak._gamesPlayed > process.env.STREAK_MAX - 1) {
			let now = moment();
			if (now.diff(playerDoc._gameStreak._streakStartTime, 'seconds') > 3600) {
				try {
					playerDoc._gameStreak._gamesPlayed = 0;
					playerDoc._gameStreak._streakStartTime = now;
					await playerDoc.save();
					next();
				} catch (err) {
					console.error(err);
					res.status(504).json({
						message: 'Gateway Timeout',
					});
				}
				next();
			} else {
				if (req.body.requestFrom) {
					res.sendStatus(401);
				}
				res.redirect('/wait');
			}
		} else {
			if (req.body.requestFrom) {
				res.sendStatus(401);
			}
			res.redirect('/wait');
		}
	}
};

exports.getGameStreak = async function (req, res, next) {
	Player.findOne(
		{ _username: req.player.name, _password: req.player.password },
		(err, result) => {
			if (err)
				res.status(504).json({
					message: 'Gateway Timeout',
				});
			res.json({ streak: result._gameStreak });
		}
	);
};

exports.checkDevice = function (req, res, next) {
	let ua = req.headers['user-agent'].toLowerCase(),
		isMobile = !!/Mobile|iP(hone|od|ad)|Android|BlackBerry|IEMobile|Kindle|NetFront|Silk-Accelerated|(hpw|web)OS|Fennec|Minimo|Opera M(obi|ini)|Blazer|Dolfin|Dolphin|Skyfire|Zune/i.test(
			ua
		);
	if (isMobile) {
		next();
	} else {
		res.status(404).json({
			message: 'Not found',
		});
	}
};

exports.addScore = async function (req, res, next) {
	if (req.body) {
		Player.findOneAndUpdate(
			{ _username: req.player.name, _password: req.player.password },
			{
				$push: {
					_scores: {
						$each: [req.body._finalScore],
						$sort: -1,
						$slice: 10,
					},
					_gameLogs: {
						$each: [req.body],
						$sort: { _finalScore: -1 },
						$slice: 3,
					},
				},
				$inc: { _totalGamesPlayed: 1, '_gameStreak._gamesPlayed': 1 },
			},
			{ safe: true, upsert: true, new: true, minimize: false },
			(err, result) => {
				if (err)
					res.status(504).json({
						message: 'Gateway Timeout',
					});
				res.status(200).json({
					bestScore: result._scores[0],
					streakCount: result._gameStreak._gamesPlayed,
				});
			}
		);
	} else
		res.status(404).json({
			message: 'Not found',
		});
};
