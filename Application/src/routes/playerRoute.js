'use strict';
import path from 'path';
import express from 'express';

module.exports = function (app) {
	var playerHandlers = require('../controllers/playerController.js');

	// app.all('*', playerHandlers.checkDevice);

	app.get(
		'/',
		playerHandlers.loginRequired,
		playerHandlers.streakValid,
		(req, res, next) => {
			res.redirect('/game');
		}
	);

	app.get('/game', playerHandlers.loginRequired, playerHandlers.streakValid);
	app.use('/game', express.static(path.join(__dirname, '../../dist-client/game')));

	app.get('/wait', (req, res, next) => {
		next();
	});
	app.use('/wait', express.static(path.join(__dirname, '../../dist-client/wait')));

	app.get('/getStreak', playerHandlers.getGameStreak);

	app.post('/newGame', playerHandlers.streakValid, (req, res, next) => {
		console.log('post(/newGame)');
		res.sendStatus(200);
	});

	app.get('/login', (req, res, next) => {
		next();
	});
	app.use('/login', express.static(path.join(__dirname, '../../dist-client/login')));
	app.post('/login', playerHandlers.login);

	app.post('/addScore', playerHandlers.addScore);
};

// express.static(path.join(__dirname, '../../client/views/game')

// app.route('/login')
// 		.get((req, res, next) => {
// 			res.sendFile('index.html', {
// 				root: path.join(__dirname, '../../client/views/other/'),
// 			});
// 		})
// 		.post(playerHandlers.login);

// app.use('/', (req, res, next) => {
// 	res.sendFile('index.html', {
// 		root: path.join(__dirname, '../../client/views/other/'),
// 	});
// });
