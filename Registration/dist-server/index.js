"use strict";

var _express = _interopRequireDefault(require("express"));

var _cors = _interopRequireDefault(require("cors"));

var _bodyParser = _interopRequireDefault(require("body-parser"));

var _path = _interopRequireDefault(require("path"));

var _regeneratorRuntime = _interopRequireDefault(require("regenerator-runtime"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var mongoose = require('mongoose');

var Player = require('./playerModel');

require('dotenv').config();

var app = (0, _express["default"])();
var port = process.env.PORT || 3000;
var uri = "mongodb+srv://dohacchan-float67:".concat(process.env.DB_PASS, "@dohacchan-fly.ev9sz.mongodb.net/").concat(process.env.DB_NAME, "?retryWrites=true&w=majority"); // Connect to DB

try {
  mongoose.connect(uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
  });
} catch (e) {
  console.error(e);
}

var MemberListSchema = new mongoose.Schema({
  _id: Number,
  _memberList: [String]
});
var MemberList = mongoose.model('MemberList', MemberListSchema, 'memberList'); //---------------------------------

app.use((0, _cors["default"])());
app.use(_bodyParser["default"].urlencoded({
  extended: false
}));
app.use(_bodyParser["default"].json());
app.get('/', function (req, res, next) {
  next();
});
app.use('/', _express["default"]["static"](_path["default"].join(__dirname, '../dist-client')));
app.post('/register', function (req, res) {
  Player.findOne({
    _username: req.body.username.toLowerCase()
  }, function (err, result) {
    if (err) {
      res.status(504).json({
        message: 'Gateway Timeout'
      });
    }

    if (result) {
      return res.status(422).json({
        message: 'User already exists'
      });
    } else {
      MemberList.findOne({
        _id: 1,
        _memberList: {
          $elemMatch: {
            $regex: req.body.username,
            $options: 'i'
          }
        }
      }, {
        _memberList: {
          $elemMatch: {
            $regex: req.body.username,
            $options: 'i'
          }
        }
      }, function (err, result) {
        if (err) {
          console.log('error = ', err);
        }

        if (result) {
          Player.create({
            _username: req.body.username.toLowerCase(),
            _realname: result._memberList[0],
            _password: req.body.password
          }, function (err, result) {
            if (err) {
              return res.status(422).json({
                message: 'Failed to Register. Try agin.'
              });
            }

            res.sendStatus(200);
          });
        } else {
          return res.status(422).json({
            message: 'Inputted User is not in the server.'
          });
        }
      });
    }
  });
});
app.listen(port, function () {
  return console.log("Example app listening on port ".concat(port, "!"));
});