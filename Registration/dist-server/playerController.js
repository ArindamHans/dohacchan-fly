"use strict";

var _mongoose = _interopRequireDefault(require("mongoose"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var Player = _mongoose["default"].model('Player');

exports.register = function (req, res) {
  Player.create({
    _username: req.body.username.toLowerCase(),
    _password: req.body.password
  }, function (err, result) {
    if (err) {
      return res.json({
        message: 'Failed to Register. Try agin.'
      });
    }

    res.send(200);
  });
};