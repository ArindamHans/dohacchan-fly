require('./site.css');
import 'normalize.css';
import $ from 'jquery';

setTimeout(function () {
	let viewheight = $(window).height();
	let viewwidth = $(window).width();
	let viewport = document.querySelector('meta[name=viewport]');
	viewport.setAttribute(
		'content',
		'height=' + viewheight + 'px, width=' + viewwidth + 'px, initial-scale=1.0'
	);
}, 100);

$(document).ready(function () {
	$('#registrationForm').on('submit', function (e) {
		e.preventDefault();
		var username = $('#username').val();
		var password = $('#password').val();

		if (username.match(/^((.+?)#\d{4})$/)) {
			if (password.length < 7) {
				$('#snackbar')
					.html('Password should be atleast 7 characters long')
					.addClass('show');
				setTimeout(function () {
					$('#snackbar').removeClass('show').addClass('');
				}, 3000);
			} else {
				$.ajax({
					type: 'POST',
					url: '/register',
					data: { 'username': username, 'password': $('#password').val() },
					success: function (xhr) {
						$('#form-container-parent').hide();
						$('body').html('Registered 👍\n \nYou can exit the tab now');
					},
					error: function (xhr) {
						var response = xhr.responseJSON;
						$('#snackbar').html(response.message).addClass('show');
						setTimeout(function () {
							$('#snackbar').removeClass('show').addClass('');
						}, 3000);
					},
				});
			}
		} else {
			$('#snackbar').html('Input a valid username').addClass('show');
			setTimeout(function () {
				$('#snackbar').removeClass('show').addClass('');
			}, 3000);
		}
	});
});
