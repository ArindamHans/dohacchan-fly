var path = require('path');
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var BrowserSyncPlugin = require('browser-sync-webpack-plugin');
var CopyWebpackPlugin = require('copy-webpack-plugin');
require('dotenv').config();

var definePlugin = new webpack.DefinePlugin({
	__DEV__: JSON.stringify(JSON.parse(process.env.BUILD_DEV || 'true')),
	WEBGL_RENDERER: true,
	CANVAS_RENDERER: true,
});

module.exports = {
	entry: {
		'app': path.resolve(__dirname, 'public/src/main.js'),
	},
	devtool: 'cheap-source-map',
	output: {
		pathinfo: true,
		path: path.resolve(__dirname, 'dev-client'),
		library: '[name]',
		libraryTarget: 'umd',
		filename: '[name].bundle.js',
	},
	watch: true,
	plugins: [
		definePlugin,
		new HtmlWebpackPlugin({
			filename: 'index.html',
			template: path.join(__dirname, '/public/src/index.html'),
			inject: true,
			chunks: ['app'],
		}),
		new BrowserSyncPlugin({
			host: 'localhost',
			port: 3333,
			server: {
				baseDir: ['./', './dev-client'],
			},
		}),
		new CopyWebpackPlugin({
			patterns: [
				{
					from: path.join(__dirname, '/public/assets'),
					to: path.join(__dirname, '/dev-client/assets'),
				},
			],
		}),
	],
	module: {
		rules: [
			{
				test: /\.m?js$/,
				exclude: /(node_modules|bower_components)/,
				use: {
					loader: 'babel-loader',
					options: {
						presets: ['@babel/preset-env'],
					},
				},
			},
			{ test: /phaser-split\.js$/, use: ['expose-loader?Phaser'] },
			{ test: [/\.vert$/, /\.frag$/], use: 'raw-loader' },
			{
				test: /\.css$/,
				use: ['style-loader', 'css-loader'],
			},
			{
				test: /\.(png|jpe?g|gif|svg)$/,
				use: [{ loader: 'url-loader' }],
			},
		],
	},
};
