import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import path from 'path';
const mongoose = require('mongoose');
const Player = require('./playerModel');
import regeneratorRuntime from 'regenerator-runtime';

require('dotenv').config();
const app = express();
const port = process.env.PORT || 4000;
const uri = `mongodb+srv://dohacchan-float67:${process.env.DB_PASS}@dohacchan-fly.ev9sz.mongodb.net/${process.env.DB_NAME}?retryWrites=true&w=majority`;

// Connect to DB
try {
	mongoose.connect(uri, {
		useNewUrlParser: true,
		useUnifiedTopology: true,
		useFindAndModify: false,
	});
} catch (e) {
	console.error(e);
}

const MemberListSchema = new mongoose.Schema({
	_id: Number,
	_memberList: [String],
});

const MemberList = mongoose.model('MemberList', MemberListSchema, 'memberList');

//---------------------------------
app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get('/', (req, res, next) => {
	next();
});
app.use('/', express.static(path.join(__dirname, '../dist-client')));
app.post('/register', (req, res) => {
	Player.findOne(
		{
			_username: req.body.username.toLowerCase(),
		},
		function (err, result) {
			if (err) {
				res.status(504).json({
					message: 'Gateway Timeout',
				});
			}
			if (result) {
				return res.status(422).json({
					message: 'User already exists',
				});
			} else {
				MemberList.findOne(
					{
						_id: 1,
						_memberList: {
							$elemMatch: { $regex: req.body.username, $options: 'i' },
						},
					},
					{
						_memberList: {
							$elemMatch: { $regex: req.body.username, $options: 'i' },
						},
					},
					function (err, result) {
						if (err) {
							console.log('error = ', err);
						}
						if (result) {
							Player.create(
								{
									_username: req.body.username.toLowerCase(),
									_realname: result._memberList[0],
									_password: req.body.password,
								},
								function (err, result) {
									if (err) {
										return res.status(422).json({
											message: 'Failed to Register. Try agin.',
										});
									}

									res.sendStatus(200);
								}
							);
						} else {
							return res.status(422).json({
								message: 'Inputted User is not in the server.',
							});
						}
					}
				);
			}
		}
	);
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
