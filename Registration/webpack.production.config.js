var path = require('path');
var webpack = require('webpack');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var CopyWebpackPlugin = require('copy-webpack-plugin');
require('dotenv').config();

var definePlugin = new webpack.DefinePlugin({
	__DEV__: JSON.stringify(JSON.parse(process.env.BUILD_DEV || 'false')),
	WEBGL_RENDERER: true,
	CANVAS_RENDERER: true,
});

module.exports = {
	entry: {
		'app': path.resolve(__dirname, 'public/src/main.js'),
	},

	output: {
		path: path.resolve(__dirname, 'dist-client'),
		filename: '[name].bundle.js',
	},

	// node: {
	// 	fs: 'empty',
	// 	net: 'empty',
	// },

	plugins: [
		definePlugin,
		new CleanWebpackPlugin(),
		new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
		/*new webpack.optimize.UglifyJsPlugin({
      drop_console: true,
      minimize: true,
      output: {
        comments: false 
      }
    }),*/
		//new webpack.optimize.CommonsChunkPlugin({ name: 'vendor' /* chunkName= */, filename: 'js/vendor.bundle.js' /* filename= */ }),
		new HtmlWebpackPlugin({
			filename: 'index.html',
			template: path.join(__dirname, '/public/src/index.html'),
			chunks: ['app'],
			minify: {
				removeAttributeQuotes: true,
				collapseWhitespace: true,
				html5: true,
				minifyCSS: true,
				minifyJS: true,
				minifyURLs: true,
				removeComments: true,
				removeEmptyAttributes: true,
			},
			hash: true,
		}),
		new CopyWebpackPlugin({
			patterns: [
				{
					from: path.join(__dirname, '/public/assets'),
					to: path.join(__dirname, '/dist-client/assets'),
				},
			],
		}),
	],
	module: {
		rules: [
			{
				test: /\.m?js$/,
				exclude: /(node_modules|bower_components)/,
				use: {
					loader: 'babel-loader',
					options: {
						presets: ['@babel/preset-env'],
					},
				},
			},
			{ test: /phaser-split\.js$/, use: 'raw-loader' },
			{ test: [/\.vert$/, /\.frag$/], use: 'raw-loader' },
			{
				test: /\.css$/,
				use: ['style-loader', 'css-loader'],
			},
			{
				test: /\.(png|jpe?g|gif|svg)$/,
				use: [{ loader: 'url-loader' }],
			},
		],
	},
	optimization: {
		minimize: true,
	},
	target: 'web',
	/*node: {
    fs: 'empty',
    net: 'empty',
    tls: 'empty'
  },
  resolve: {
    alias: {
      'phaser': phaser,

    }
  }*/
};
