const mongoose = require('mongoose');

let Schema = mongoose.Schema;

const ScoreboardSchema = new Schema({
	_username: {
		type: String,
		match: /^((.+?)#\d{4})$/,
	},
	_realname: {
		type: String,
		match: /^((.+?)#\d{4})$/,
	},
	_topScore: Number,
	_rank: Number,
});

module.exports = mongoose.model('Scoreboard', ScoreboardSchema, 'scoreBoard');
