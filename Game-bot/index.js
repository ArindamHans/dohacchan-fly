require('dotenv').config();
const Discord = require('discord.js');
const mongoose = require('mongoose');
const moment = require('moment');

moment().format();
const Player = require('./playerModel');
const Scoreboard = require('./scoreboardModel');
const bot = new Discord.Client();
const TOKEN = process.env.TOKEN;
const CHANNEL_ID = process.env.CHANNEL_ID;
const uri = `mongodb+srv://dohacchan-float67:${process.env.DB_PASS}@dohacchan-fly.ev9sz.mongodb.net/${process.env.DB_NAME}?retryWrites=true&w=majority`;

try {
	mongoose.connect(uri, {
		useNewUrlParser: true,
		useUnifiedTopology: true,
		useFindAndModify: false,
	});
} catch (e) {
	console.error(e);
}

bot.login(TOKEN);

bot.on('ready', () => {
	console.info(`Logged in as ${bot.user.tag}!`);
});

const MemberListSchema = new mongoose.Schema({
	_id: Number,
	_memberList: [String],
});

const MemberList = mongoose.model('MemberList', MemberListSchema, 'memberList');

var updateMemberList = setInterval(function () {
	let memberArr = [];
	const list = bot.guilds.get(process.env.GUILD_ID);
	list.members.forEach((member) => memberArr.push(member.user.tag));
	MemberList.deleteMany({}, function (err) {
		if (err) console.log(err);

		MemberList.create({ _id: 1, _memberList: memberArr }, function (err, result) {
			if (err) console.log(err);

			console.log('result = ', result);
		});
	});
}, process.env.NEW_MEMBER_RENEW_TIME);

var updateScoreboard = setInterval(function () {
	Player.find({}, { _username: 1, _realname: 1, _scores: { $slice: 1 } })
		.sort({ _scores: -1 })
		.exec(function (err, result) {
			if (err) console.error(err);

			var i = 1;
			Scoreboard.deleteMany({}, function (err) {
				if (err) console.log(err);
				console.log('DELETED');
				console.log('🍑 player = ', result);
				result.forEach(function (player) {
					Scoreboard.create(
						{
							_username: player._username,
							_realname: player._realname,
							_topScore: player._scores[0],
							_rank: i++,
						},
						function (err, result) {
							if (err) console.log(err);
							console.log('result = ', result);
						}
					);
				});
			});
		});
}, process.env.SCOREBOARD_RENEW_TIME);

bot.on('message', (msg) => {
	if (/^!rank\s?([2-9]|10)?$/.test(msg.content) && msg.channel.id === CHANNEL_ID) {
		Player.findOne({ _username: msg.member.user.tag.toLowerCase() }, function (
			err,
			result
		) {
			if (err) {
				msg.reply("You're not registered.");
			}

			try {
				Scoreboard.findOne({ _username: result._username }, function (
					err,
					scoreboardData
				) {
					if (err) console.error(err);

					var msgText = ` \n> **Rank :** ${scoreboardData._rank} \
        							\n> **Best Score :** ${result._scores[0]} \
									\n> **Matches Played :** ${result._totalGamesPlayed}`;

					if (result._gameStreak._gamesPlayed == process.env.STREAK_MAX) {
						const second = 1000,
							minute = second * 60,
							hour = minute * 60;

						var endDate = new Date(result._gameStreak._streakStartTime);
						endDate.setHours(endDate.getHours() + 1);
						var endTime = endDate.getTime();

						var now = new Date().getTime(),
							distance = endTime - now;

						var minLeft = Math.floor((distance % hour) / minute);
						var secLeft = Math.floor((distance % minute) / second);

						msgText =
							msgText +
							`\n> **Streak :** 0 **/** ${process.env.STREAK_MAX}`;

						if (minLeft > 0) {
							msgText =
								msgText +
								`\n> **Cooldown in :** ${minLeft} **.** ${secLeft} mins`;
						}
					} else {
						msgText =
							msgText +
							`\n> **Streak :** ${result._gameStreak._gamesPlayed} **/** ${process.env.STREAK_MAX}`;
					}

					if (/([2-9]|10)/.test(msg.content.slice(5))) {
						limit_num = parseInt(
							msg.content.slice(5, 9).match(/([2-9]|10)/)[0]
						);
						msgText = msgText + `\n> **Scores :** \n> **[** `;
						for (let i = 0; i < limit_num; i++) {
							msgText = msgText + ` **${result._scores[i]}**  | `;
						}
						msgText = msgText.slice(0, -2) + ` **]**`;
					}

					msg.reply(msgText);
				});
			} catch (err) {
				console.error(err);
				msg.channel.send('☹ Timeout');
			}
		});
	}
	if (
		(/^!global\s?([5-9]|10)?$/.test(msg.content) ||
			/^!ranks\s?([5-9]|10)?$/.test(msg.content)) &&
		msg.channel.id === CHANNEL_ID
	) {
		if (/([5-9]|10)/.test(msg.content.slice(6, 10))) {
			limit_num = parseInt(msg.content.slice(6, 10).match(/([5-9]|10)/)[0]);
		} else limit_num = 5;

		var msgText = '';
		Scoreboard.find({})
			.sort({ _rank: 1 })
			.limit(limit_num)
			.exec(function (err, result) {
				if (err) console.error(err);

				var rankEmoji = [
					'1️⃣',
					'2️⃣',
					'3️⃣',
					'4️⃣',
					'5️⃣',
					'6️⃣',
					'7️⃣',
					'8️⃣',
					'9️⃣',
					'🔟',
				];
				i = 0;
				result.forEach(function (player) {
					msgText =
						msgText +
						`\n> ${rankEmoji[i++]}  **${player._realname}**   |   **${
							player._topScore
						}**`;
				});
				console.log(msgText);
				msg.channel.send(msgText);
			});
	}
	if (msg.content == '!help' && msg.channel.id === CHANNEL_ID) {
		var msgText = `> **!help**  **|**  Shows this message \
						 \n> **!rank [2-10]**  **|**  Shows your stats \
						 \n> **!ranks** or **!global**  **[5-10]**  **|**  Scoreboard`;
		msg.channel.send(msgText);
	}
});
