const mongoose = require('mongoose');
let Schema = mongoose.Schema;

const PlayerSchema = new Schema({
	_username: {
		type: String,
		required: true,
		match: /^((.+?)#\d{4})/,
	},
	_realname: {
		type: String,
		match: /^((.+?)#\d{4})/,
	},
	_password: {
		type: String,
		required: true,
	},
	_scores: {
		type: [Number],
		default: new Array(10).fill(0),
	},
	_totalGamesPlayed: {
		type: Number,
		default: 0,
	},
	_gameStreak: {
		_streakStartTime: {
			type: Date,
			default: Date.now(),
		},
		_gamesPlayed: {
			type: Number,
			default: 0,
		},
	},
});

module.exports = mongoose.model('Player', PlayerSchema, 'players');
